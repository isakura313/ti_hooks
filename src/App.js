import './App.css';
import  React from 'react';

// import Clock from './Clock';
import Navigator  from './Navigator';

function App() {
  return (
    <div className="App">
      <Navigator/>
    </div>
  );  
}

export default App;

import React, { useState, useEffect } from 'react';


function toZeroFunction(number){
  // this function have add 0 when number
  let n = +number;
  if(n < 10){
    return '0'+ n
  } else{
    return n
  }
};
function Clock() {
  const [hours, setHours] = useState(toZeroFunction(new Date().getHours()));
  const [minutes, setMinutes] = useState(toZeroFunction(new Date().getMinutes()));
  const [seconds, setSeconds] = useState(toZeroFunction(new Date().getSeconds()));

  // Аналогично componentDidMount и componentDidUpdate:
  useEffect(() => {
    // Обновляем заголовок документа с помощью API браузера
    let timer = setTimeout(()=>{
      setHours(toZeroFunction(new Date().getHours()))
      clearTimeout(timer);
    },1000)
    let timer2 = setTimeout(()=>{
      setMinutes(toZeroFunction(new Date().getMinutes()))
      clearTimeout(timer2);
    },1000)
    let timer3 = setTimeout(()=>{
      setSeconds(toZeroFunction(new Date().getSeconds()))
      clearTimeout(timer3);
    },1000)
  });

  return (
    <div>
      <p> сейчас <span>{hours} : </span><span>{minutes}</span> <span> : {seconds}</span></p>
    </div>
  );
}

export default Clock
import {
    BrowserRouter as Router,
    Routes,
    Route,
    Link
} from "react-router-dom";
import Clock from './Clock';


function Navigator() {
    return (
        <Router>
            <div>
                <nav>
                    <ul>
                        <li>
                            <Link to="/">Clock</Link>
                        </li>
                        <li>
                            <Link to="/about">About</Link>
                        </li>
                        </ul>
                </nav>

                {/* A <Routes> looks through its children <Route>s and
                renders the first one that matches the current URL. */}
                <Routes>
                    <Route path="/" element={Clock}>
                    </Route>
                </Routes>
            </div>
        </Router>
    );
}
export default Navigator